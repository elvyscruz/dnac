// checks domain name availability from file
let fs = require('fs')
let exec = require('child_process').exec
fs.readFile('words.txt', 'utf8', function (err, data) {
  if (err) throw err
  let words = data.split('\n')
  words.forEach(function (word) {
    console.log(`verifying ${word}`)
    exec(`host ${word}.com`, function (err, stdout, stderr) {
      if (/found/.test(stdout)) {
        fs.appendFile('available.txt', `${word} is available`, function (err) {
          if (err) throw err
          console.log(`${word} is available`)
        })
      }
    })
  })
})
